import withAuth from "@/common/hooks/useAuth";
import { atom } from "jotai";
import { useNavigate } from "react-router-dom";
import { usePostQuizAnswer } from "./answer-api";
import { Dialog, Form, Input, NavBar, Toast } from "antd-mobile";
import TokenContoller from "@/common/TokenController";
import { useRef } from "react";
import { useForm, Controller } from "react-hook-form";
import { FormInstance } from "antd-mobile/es/components/form";
import _ from "underscore";
import "@dotlottie/player-component";
import SendIcon from "@/common/assets/send.svg";
import { DialogShowHandler } from "antd-mobile/es/components/dialog";

export const atom_showGlobalLoading = atom<boolean>(false);

const QuizAnswerPage = () => {
  const navigate = useNavigate();
  const postQuizAnswerQuery = usePostQuizAnswer();
  const handler = useRef<DialogShowHandler>();
  const formRef = useRef<FormInstance>(null);

  const { control, handleSubmit, reset, getValues } = useForm();

  const onSubmit = (data) => {
    postQuizAnswerQuery
      .mutateAsync({
        answer: data.answer,
      })
      .then((res) => {
        if (
          res.code === "updated_sub_answer" ||
          res.code === "updated_main_answer"
        ) {
          Toast.show({
            icon: 'success',
            content: res.message,
          });
        }
      })
      .catch((err) => {
        const data = err.response.data;
        if (_.has(data, "message")) {
          console.log(data.message);
          return Toast.show({
            icon: 'fail',
            content: data.message,
          });
        }
        return Toast.show({
          icon: 'fail',
          content: "알 수 없는 에러가 발생했습니다. 새로고침 해주세요",
        });
      })
      .finally(() => {
        reset({
          answer: "",
        });
      });
  };

  const forceSubmit = () => {
    if (!formRef || !formRef.current) return;
    formRef.current.submit();
  };

  const 답변제출확인모달오픈핸들러 = () => {
    const values = getValues();
    if (!values.answer) return;

    handler.current = Dialog.show({
      title: "정말 제출하시겠습니까?",
      content:
        "제출 이후에는 변경할 수 없습니다 (변경이 필요한 경우 관리자에게 문의해주세요)",
      actions: [
        [
          {
            key: "cancel",
            text: "취소",
          },
          {
            key: "yes",
            text: "확인",
            bold: true,
            danger: true,
          },
        ],
      ],
      closeOnMaskClick: true,
      onAction: (action) => {
        if (action.key === "yes") {
          handler.current?.close();
          forceSubmit();
        }
        if (action.key === "cancel") {
          handler.current?.close();
        }
      },
    });
  };

  return (
    <div className="flex flex-col h-full answer-page">
      <NavBar
        onBack={() => {
          TokenContoller.clear();
          navigate("/my-login");
        }}
      ></NavBar>
      <div className="flex-1 flex justify-center items-center flex-col pb-[10vh]">
        <dotlottie-player
          src="https://assets-v2.lottiefiles.com/a/46e61ed0-1188-11ee-a16b-77ba46944b28/I7oWav0UeL.lottie"
          autoplay
          loop
          style={{ height: "300px", width: "300px" }}
        />
        <Form
          className="w-full"
          onFinish={handleSubmit(onSubmit)}
          ref={formRef}
        >
          <Controller
            name="answer"
            control={control}
            render={({ field: { ref, ...field } }) => (
              <Form.Item>
                <div className="flex items-center">
                  <div className="flex-1">
                    <Input
                      {...field}
                      ref={ref}
                      placeholder="단어 입력"
                      clearable
                      style={{ "--font-size": "22px" }}
                      className="text-center"
                    />
                  </div>
                  <button
                    type="button"
                    onClick={답변제출확인모달오픈핸들러}
                    disabled={postQuizAnswerQuery.isPending}
                  >
                    <img src={SendIcon} />
                  </button>
                </div>
              </Form.Item>
            )}
          ></Controller>
        </Form>
      </div>
    </div>
  );
};

export default withAuth(QuizAnswerPage);
