import axios, { AxiosError, type AxiosResponse } from "axios";
import TokenContoller from "../TokenController";
const axiosConfig = {
  baseURL: import.meta.env.VITE_API_URL,
  headers: {
    "Content-Type": "application/json",
  },
};

const axiosInstance = axios.create(axiosConfig);
axiosInstance.defaults.timeout = 10000;

const handleAxiosError = (
  error: AxiosError<{ message: string; code: string; data: any }>
) => {
  if (error.response?.status === 403) {
    TokenContoller.clear();
    window.location.replace("/");
    return Promise.reject(error);
  }

  const data = error.response?.data;
  if (data?.message) {
    console.error(data.message);
    console.log(data.code);

    if (data.code === "rest_invalid_param") {
      if (data.data) {
        const keys = Object.keys(data.data.params);
        const errorMessage = data.data.params[keys[0]];
        error.response!.data.message = errorMessage;
        return Promise.reject(error);
      }
    }
    // TODO: 커스텀 에러 코드를 만들어서 그에 맞는 message를 담은 error 객체를 return 하도록 해야 함
    return Promise.reject(error);
  }

  error.message =
    "알 수 없는 오류가 발생했습니다. 잠시 후 다시 시도해주세요 :(";
  return Promise.reject(error);
};

const handleAxiosResponse = (response: AxiosResponse) => {
  if (response.data) return response;

  // 서버에서 아무 응답 데이터도 오지 않으면 빈 스트링 ''이 오므로 명시적으로 null로 지정
  response.data = null;
  return response;
};

axiosInstance.interceptors.response.use(handleAxiosResponse, handleAxiosError);

axiosInstance.interceptors.request.use(
  async (config) => {
    const token = TokenContoller.getToken();
    if (!token) {
      console.error("토큰이 없습니다.");
      return config;
    }

    config.headers["Authorization"] = `Bearer ${token}`;
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export { axiosInstance as default };
