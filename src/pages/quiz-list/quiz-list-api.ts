import axiosInstance from "@/common/api/axiosInstance";
import { API, Quiz } from "@/common/types";
import { useQuery } from "@tanstack/react-query";
import { AxiosResponse } from "axios";

export type ApiGetQuizList = API<
  unknown,
  {
    quiz_list: Quiz[];
    total_count: number;
    current_page: number;
  }
>;

export const getQuizList = async () => {
  const url = `${import.meta.env.VITE_API_URL}/quiz/get-quiz-list`;
  const response = await axiosInstance.get<
    ApiGetQuizList["params"],
    AxiosResponse<ApiGetQuizList["response"]>
  >(url, {
    timeout: 5000,
  });

  return response.data;
};

export const useGetQuizList = () => {
  const key = ["get-quiz-list"].filter((item) => !!item);
  return useQuery<ApiGetQuizList["response"]>({
    queryKey: key,
    queryFn: () => getQuizList(),
    staleTime: 1000 * 60 * 10, // 10분 동안 캐싱. 공지사항은 잘 올라오지 않기 때문에 10분이 적당하다
    gcTime: 1000 * 60 * 10, // 10분 동안 캐싱. 공지사항은 잘 올라오지 않기 때문에 10분이 적당하다
  });
};
