import withAuth from "@/common/hooks/useAuth";
import { atom } from "jotai";
import { useNavigate, useParams } from "react-router-dom";
import { useGetQuizDetail, usePostQuizAnswer } from "./quiz-detail-api";
import GlobalLoading from "@/components/@shared/global-loading/GlobalLoading";
import ErrorPage from "../error/_ErrorPage";
import { Button, Dialog, Form, Input, NavBar } from "antd-mobile";
import TokenContoller from "@/common/TokenController";
import { useMemo, useRef } from "react";
import { useForm, Controller } from "react-hook-form";
import { toast } from "react-hot-toast";
import { DialogShowHandler } from "antd-mobile/es/components/dialog";
import { FormInstance } from "antd-mobile/es/components/form";
import _ from "underscore";

export const atom_showGlobalLoading = atom<boolean>(false);

const QuizDetailPage = () => {
  const navigate = useNavigate();
  const params = useParams();
  const quizId = params["id"]!;
  const getQuizDetailQuery = useGetQuizDetail(quizId, !!quizId);
  const userData = useMemo(() => TokenContoller.getUserData(), []);
  const postQuizAnswerQuery = usePostQuizAnswer();
  const handler = useRef<DialogShowHandler>();
  const formRef = useRef<FormInstance>(null);

  const { control, handleSubmit, reset } = useForm();

  if (getQuizDetailQuery.isLoading) {
    return <GlobalLoading visible={true} />;
  }

  if (getQuizDetailQuery.isError) {
    return <ErrorPage />;
  }

  const quiz = getQuizDetailQuery.data!.data;
  const isMainTeam = userData
    ? userData.user_email === quiz.main_team_user_email
    : false;

  const onSubmit = (data) => {
    postQuizAnswerQuery
      .mutateAsync({
        quiz_id: quizId,
        main_answer: data.main_answer,
        sub_answer: data.sub_answer,
      })
      .then((res) => {
        if (
          res.code === "updated_main_answer" ||
          res.code === "updated_sub_answer"
        ) {
          toast.success(res.message);
          getQuizDetailQuery.refetch();
          reset({
            main_answer: "",
            sub_answer: "",
          });
        }
      })
      .catch((err) => {
        const data = err.response.data;
        if (_.has(data, "message")) {
          return toast.error(data.message);
        }
        return toast.error("알 수 없는 에러가 발생했습니다. 새로고침 해주세요");
      });
  };

  const forceSubmit = () => {
    if (!formRef || !formRef.current) return;
    formRef.current.submit();
  };

  const 답변제출확인모달오픈핸들러 = () => {
    handler.current = Dialog.show({
      title: "정말 제출하시겠습니까?",
      content:
        "제출 이후에는 변경할 수 없습니다 (변경이 필요한 경우 관리자에게 문의해주세요)",
      actions: [
        [
          {
            key: "cancel",
            text: "취소",
          },
          {
            key: "yes",
            text: "확인",
            bold: true,
            danger: true,
          },
        ],
      ],
      closeOnMaskClick: true,
      onAction: (action) => {
        if (action.key === "yes") {
          handler.current?.close();
          forceSubmit();
        }
        if (action.key === "cancel") {
          handler.current?.close();
        }
      },
    });
  };

  return (
    <div>
      <NavBar onBack={() => navigate("/quiz-list")}>퀴즈</NavBar>
      <div className="inner py-4 px-4 min-h-[30vh]">
        <h1>{quiz.title}</h1>
        <p className="text-xl">{quiz.content}</p>
      </div>
      <div className="">
        <Form
          onFinish={handleSubmit(onSubmit)}
          ref={formRef}
          footer={
            <Button
              className="!mb-2"
              block
              type="button"
              color="primary"
              size="large"
              disabled={
                getQuizDetailQuery.isLoading ||
                getQuizDetailQuery.isError ||
                !quiz.active
              }
              loading={
                getQuizDetailQuery.isLoading || postQuizAnswerQuery.isPending
              }
              onClick={답변제출확인모달오픈핸들러}
            >
              제출하기
            </Button>
          }
        >
          <Form.Header>답변 입력</Form.Header>
          <Controller
            name="main_answer"
            control={control}
            render={({ field: { ref, ...field } }) => (
              <Form.Item
                label={`주인공 답변 (현재: ${quiz.main_answer || "미지정"})`}
              >
                <Input
                  {...field}
                  ref={ref}
                  placeholder="주인공 답변을 입력해주세요"
                  clearable
                  disabled={!isMainTeam || !quiz.active}
                />
              </Form.Item>
            )}
          ></Controller>
          <Controller
            name="sub_answer"
            control={control}
            render={({ field: { ref, ...field } }) => (
              <Form.Item label="관객 답변">
                <Input
                  {...field}
                  ref={ref}
                  placeholder="관객 답변을 입력해주세요"
                  clearable
                  disabled={!!isMainTeam || !quiz.active}
                />
              </Form.Item>
            )}
          ></Controller>
        </Form>
      </div>
    </div>
  );
};

export default withAuth(QuizDetailPage);
