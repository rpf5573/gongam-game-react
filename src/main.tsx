import React from "react";
import ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import "./common/scss/style.scss";
import { ConfigProvider } from "antd-mobile";
import koKR from "antd-mobile/es/locales/ko-KR";

import LoginPage from "@pages/login/_LoginPage.tsx";
import { ErrorBoundary } from "react-error-boundary";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import GlobalError from "@components/@shared/global-error/GlobalError";
import { Toaster } from "react-hot-toast";
import QuizAnswerPage from "@pages/answer/_AnswerPage.tsx";

const router = createBrowserRouter([
  {
    path: "/*",
    element: <QuizAnswerPage />,
  },
  {
    path: "/my-login",
    element: <LoginPage />,
  },
]);

// Create a client
const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      retry: 3,
      suspense: false,
      refetchOnWindowFocus: false, // default: true
      retryDelay: 200,
      staleTime: 0, // 캐싱을 완전히 하지 않는다
      gcTime: 0,
    },
  },
});

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <ErrorBoundary FallbackComponent={GlobalError}>
      <ConfigProvider locale={koKR}>
        <QueryClientProvider client={queryClient}>
          <RouterProvider router={router} />
          <Toaster containerStyle={{
            top: '48vh',
          }} />
        </QueryClientProvider>
      </ConfigProvider>
    </ErrorBoundary>
  </React.StrictMode>
);
