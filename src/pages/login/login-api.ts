import axiosInstance from "@/common/api/axiosInstance";
import { API } from "@/common/types";
import type { DefaultError } from "@tanstack/query-core";
import { useMutation, useQuery } from "@tanstack/react-query";
import { AxiosResponse } from "axios";

export type ApiPostLogin = API<
  {
    password: string;
  },
  {
    token: string;
    user_display_name: string;
    user_email: string;
    user_nicename: string;
  }
>;

// login
export const postLogin = async ({ password }: ApiPostLogin["params"]) => {
  const response = await axiosInstance.post<
    ApiPostLogin["response"],
    AxiosResponse<ApiPostLogin["response"]>,
    ApiPostLogin["variables"]
  >(
    `${import.meta.env.VITE_API_URL}/login`,
    { password },
    {
      timeout: 5000,
    }
  );

  return response.data;
};

export const usePostLogin = () =>
  useMutation<
    ApiPostLogin["response"],
    DefaultError,
    ApiPostLogin["variables"]
  >({
    mutationFn: postLogin,
  });

export default usePostLogin;

type ApiGetLogoURL = API<unknown, { logo_url: string }>;

export const getLogoUrl = async () => {
  const response = await axiosInstance.get<ApiGetLogoURL["response"]>(
    `${import.meta.env.VITE_API_URL}/logo/get-logo-url`,
    {
      timeout: 5000,
    }
  );

  return response.data;
};

export const useGetLogoUrl = () => {
  return useQuery<ApiGetLogoURL["response"]>({
    queryKey: ["get-logo-url"],
    queryFn: () => getLogoUrl(),
  });
};
