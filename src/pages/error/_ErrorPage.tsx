import TokenContoller from "@/common/TokenController";
import { Result } from "antd";
import { Button } from "antd-mobile";
const ErrorPage = () => {
  const handleLogoutButtonClick = () => {
    TokenContoller.clear();
    window.location.href = "/";
  };

  return (
    <Result
      status="500"
      title="500"
      subTitle="알수없는 오류가 발생했습니다"
      extra={
        <div>
          <Button color="primary" onClick={handleLogoutButtonClick}>
            홈으로 돌아가기
          </Button>
        </div>
      }
    />
  );
};
export default ErrorPage;
