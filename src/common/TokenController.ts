import type { ApiPostLogin } from "@pages/login/login-api";
import _ from "underscore";

class TokenContoller {
  private static USER_DATA_KEY = "gongam-game";
  // private static TOKEN_DATE_TIME_KEY = "vendor-apmmust-token-expire-time";

  static isValidUserData = (
    userData: unknown
  ): userData is ApiPostLogin["response"]["data"] => {
    const isValid =
      _.isObject(userData) &&
      _.has(userData, "token") &&
      _.has(userData, "user_display_name") &&
      _.has(userData, "user_email") &&
      _.has(userData, "user_nicename");

    return isValid;
  };

  static getToken() {
    const userData = this.getUserData();
    if (!userData) return null;
    if (!userData.token) return null;
    return userData.token;
  }

  static getUserData() {
    // const result = JSON.parse(window.localStorage.getItem(this.USER_DATA_KEY) ?? "{}");
    const result = window[this.USER_DATA_KEY] ?? {};
    if (this.isValidUserData(result)) {
      return result;
    }
    return null;
  }

  static saveUserData(userData: unknown) {
    if (!this.isValidUserData(userData)) {
      throw new Error("user data is not valid");
    }
    // window.localStorage.setItem(this.USER_DATA_KEY, JSON.stringify(userData));
    window[this.USER_DATA_KEY] = userData;
  }

  static clear() {
    window[this.USER_DATA_KEY] = null;
    // window.localStorage.removeItem(this.USER_DATA_KEY);
  }
}

export default TokenContoller;
