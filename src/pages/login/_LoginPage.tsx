import TokenContoller from "@/common/TokenController";
import { useGetLogoUrl, usePostLogin } from "./login-api";
import withAuth from "@/common/hooks/useAuth";
import { Button, Form, Input } from "antd-mobile";
import React from "react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import _ from "underscore";
// import { message } from "antd";
import JSConfetti from "js-confetti";
import "animate.css";

const jsConfetti = new JSConfetti();

const LoginPage: React.FC = () => {
  const { mutateAsync: login } = usePostLogin();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [form] = Form.useForm();
  const navigate = useNavigate();
  const getLogoUrlQuery = useGetLogoUrl();

  const handleSubmit = () => {
    setIsLoading(true);

    const values = form.getFieldsValue();
    const isValid = _.isObject(values) && _.has(values, "my_password");
    if (!isValid) return;
    const { my_password: password } = values;

    login({ password })
      .then((response) => {
        try {
          jsConfetti.addConfetti();
          setTimeout(() => {
            jsConfetti.addConfetti();
            setTimeout(() => {
              jsConfetti.addConfetti();
              setTimeout(() => {
                jsConfetti.addConfetti();
              }, 300);
            }, 300);
          }, 300);
          setTimeout(() => {
            const userData = response.data;
            TokenContoller.saveUserData(userData);
            setIsLoading(false);
            // message.success("성공적으로 로그인 되었습니다");
            navigate("/notice-list");
          }, 2000);
        } catch (err) {
          console.log(err);
          // toast.error("알수없는 오류가 발생했습니다");
          setIsLoading(false);
        }
      })
      .catch((err) => {
        console.log("ERRROR ", err);
        // const { response: { data } } = err;
        // const errorMessage = data?.message ?? "알수없는 오류가 발생했습니다";
        // message.error(errorMessage);
        setIsLoading(false);
      });
  };

  if (getLogoUrlQuery.isLoading || getLogoUrlQuery.isError) return null;

  return (
    <div className="flex flex-col justify-center h-[100vh]">
      <div className="p-5">
        <div className="logo-container animate__animated animate__bounceInDown">
          <img
            src={getLogoUrlQuery.data!.data.logo_url}
            className="logo dark-logo m-auto"
            alt="apM MUST"
            width="250px"
          />
        </div>
      </div>
      <Form
        form={form}
        layout="horizontal"
        className="pt-[10vh]"
        footer={
          <>
            <Button
              loading={isLoading}
              block
              color="primary"
              size="large"
              className="mt-1"
              onClick={handleSubmit}
            >
              로그인
            </Button>
          </>
        }
      >
        <Form.Item name="my_password">
          <Input
            placeholder="입장"
            type="password"
            style={{ "--font-size": "22px" }}
            className="text-center"
          />
        </Form.Item>
      </Form>
    </div>
  );
};

export default withAuth(LoginPage);
