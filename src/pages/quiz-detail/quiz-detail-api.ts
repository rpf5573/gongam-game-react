import axiosInstance from "@/common/api/axiosInstance";
import { API, Quiz } from "@/common/types";
import { DefaultError, useMutation, useQuery } from "@tanstack/react-query";
import { AxiosResponse } from "axios";

type ApiGetQuizDetail = API<
  { notice_id: string },
  Quiz & {
    main_team_user_id: string;
    main_team_user_email: string;
    main_answer: string | undefined;
    active: boolean;
  }
>;

type ApiPostQuizAnswer = API<
  {
    quiz_id: string;
    main_answer: string | undefined;
    sub_answer: string | undefined;
  },
  unknown
>;

export const getQuizDetail = async (quizId: string) => {
  const response = await axiosInstance.get<ApiGetQuizDetail["response"]>(
    `${import.meta.env.VITE_API_URL}/quiz/get-quiz-detail?quiz_id=${quizId}`,
    {
      timeout: 3000,
    }
  );

  return response.data;
};

export const useGetQuizDetail = (quizId: string, enabled: boolean = true) => {
  return useQuery<ApiGetQuizDetail["response"]>({
    queryKey: ["get-notice-detail", quizId],
    queryFn: () => getQuizDetail(quizId),
    enabled,
  });
};

export const getQuizAnswerPermission = async (quizId: string) => {
  const response = await axiosInstance.get(
    `${
      import.meta.env.VITE_API_URL
    }/quiz/get-quiz-answer-permission?quiz_id=${quizId}`,
    {
      timeout: 3000,
    }
  );

  return response.data;
};

export const postQuizAnswer = async ({
  quiz_id,
  main_answer,
  sub_answer,
}: ApiPostQuizAnswer["params"]) => {
  const response = await axiosInstance.post<
    ApiPostQuizAnswer["response"],
    AxiosResponse<ApiPostQuizAnswer["response"]>,
    ApiPostQuizAnswer["variables"]
  >(
    `${import.meta.env.VITE_API_URL}/quiz/submit-quiz-answer`,
    {
      quiz_id: quiz_id,
      main_answer,
      sub_answer,
    },
    {
      timeout: 3000,
    }
  );

  console.log("response", response);

  return response.data;
};

export const usePostQuizAnswer = () =>
  useMutation<
    ApiPostQuizAnswer["response"],
    DefaultError,
    ApiPostQuizAnswer["variables"]
  >({
    mutationFn: postQuizAnswer,
  });
