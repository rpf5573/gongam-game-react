export type API<Params, ResponseData> = {
  params: Params;
  response: {
    code: string;
    message: string;
    data: ResponseData;
  };
  variables: Params;
};

export type Quiz = {
  id: string;
  title: string;
  content: string;
};
