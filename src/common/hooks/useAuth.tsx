import TokenContoller from "@/common/TokenController";
import * as React from "react";
import { Navigate, useLocation } from "react-router-dom";

const withAuth = (Component: React.FC<any>) => {
  const Auth = () => {
    const location = useLocation();
    const token = TokenContoller.getToken();

    // login page에 들어왔는데 이미 로그인이 되어있다면, HOME으로 돌린다
    if (token && location.pathname === "/my-login") {
      return <Navigate to={{ pathname: "/" }} />;
    }

    // login페이지 인데 jwt가 없다면, 계속 redirect되면 안되니까 여기서 끊어준다
    if (location.pathname === "/my-login" && !token) {
      return <Component />; // 여기는 Login Component가 되겠지
    }

    // login페이지도 아니고, jwt도 없다면 login페이지로 돌린다
    if (!token) {
      return <Navigate to={{ pathname: "/my-login" }} />;
    }

    // jwt도 있고, 로그인 페이지도 아니라면 이동하려던 컴포넌트로 이동한다
    return <Component />;
  };

  return Auth;
};

export default withAuth;
