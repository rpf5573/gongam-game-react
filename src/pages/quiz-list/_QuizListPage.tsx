import withAuth from "@/common/hooks/useAuth";
import { List } from "antd-mobile";
import { atom } from "jotai";
import { useNavigate } from "react-router-dom";
import { useGetQuizList } from "./quiz-list-api";
import GlobalLoading from "@/components/@shared/global-loading/GlobalLoading";
import ErrorPage from "../error/_ErrorPage";

export const atom_showGlobalLoading = atom<boolean>(false);

const QuizListPage = () => {
  const navigate = useNavigate();
  const getQuizListQuery = useGetQuizList();

  if (getQuizListQuery.isLoading) {
    return <GlobalLoading visible={true} />;
  }

  if (getQuizListQuery.isError) {
    return <ErrorPage />;
  }

  const quizList = getQuizListQuery.data?.data?.quiz_list || [];

  return (
    <List header="퀴즈 리스트">
      {quizList.map((quiz) => {
        return (
          <List.Item
            key={quiz.id}
            onClick={() => {
              navigate(`/quiz-detail/${quiz.id}`);
            }}
          >
            {quiz.title}
          </List.Item>
        );
      })}
    </List>
  );
};

export default withAuth(QuizListPage);
