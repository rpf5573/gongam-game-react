type CustomFormItemProps = {
  label?: string;
  required?: boolean;
  showError?: boolean;
  errorMessage?: string;
  children?: React.ReactNode;
};

const CustomFormItem: React.FC<CustomFormItemProps> = (props) => {
  const { label, required, showError, errorMessage, children } = props;
  return (
    <div className="adm-list-item adm-form-item adm-form-item-vertical">
      <div className="adm-list-item-content">
        <div className="adm-list-item-content-main">
          {label && (
            <div className="adm-list-item-title">
              <label
                className="adm-form-item-label"
                htmlFor="featured_image_url"
              >
                {required && (
                  <span className="adm-form-item-required-asterisk">*</span>
                )}
                {label}
              </label>
            </div>
          )}
          <div className="adm-form-item-child adm-form-item-child-position-normal">
            <div className="adm-form-item-child-inner">{children}</div>
          </div>
          <div className="adm-list-item-description">
            {showError && (
              <div className="adm-form-item-feedback-error">
                {errorMessage ?? "다시 확인해 주세요"}
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default CustomFormItem;
