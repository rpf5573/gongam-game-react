import { SpinLoading } from "antd-mobile";
import "./global-loading.scss";

type GlobalLoadingProps = {
  visible: boolean;
};

const GlobalLoading: React.FC<GlobalLoadingProps> = ({ visible }) => {
  if (!visible) return null;
  return (
    <div className="global-loading">
      <div className="dimmer"></div>
      <div className="spinner">
        <SpinLoading color="primary" />
      </div>
    </div>
  );
};

export default GlobalLoading;
