import axiosInstance from "@/common/api/axiosInstance";
import { API } from "@/common/types";
import { DefaultError, useMutation } from "@tanstack/react-query";
import { AxiosResponse } from "axios";

type ApiPostQuizAnswer = API<
  {
    answer: string;
  },
  unknown
>;

export const postQuizAnswer = async ({
  answer,
}: ApiPostQuizAnswer["params"]) => {
  const response = await axiosInstance.post<
    ApiPostQuizAnswer["response"],
    AxiosResponse<ApiPostQuizAnswer["response"]>,
    ApiPostQuizAnswer["variables"]
  >(
    `${import.meta.env.VITE_API_URL}/quiz/submit-quiz-answer`,
    {
      answer,
    },
    {
      timeout: 3000,
    }
  );

  return response.data;
};

export const usePostQuizAnswer = () =>
  useMutation<
    ApiPostQuizAnswer["response"],
    DefaultError,
    ApiPostQuizAnswer["variables"]
  >({
    mutationFn: postQuizAnswer,
  });
